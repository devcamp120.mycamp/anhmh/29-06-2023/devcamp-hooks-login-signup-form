
import { Grid, ButtonGroup, Button } from '@mui/material';
import { Container } from '@mui/system';
import { useState } from 'react';
import Login from './components/Login';
import Signup from './components/Signup';

function App() {
  const [isLogIn, setIsLogIn] = useState(true);
  return (
    <div>
      <Container>
        <Grid container style={{ display: "flex", justifyContent: "center", height: "100vh" }}>
          <Grid item xs={12} sm={12} md={6} lg={6} mt={3} sx={{ backgroundColor: "#c0bdb9" }} padding={5} >
            <ButtonGroup variant="contained" fullWidth mt={5} aria-label="outlined primary button group">
              <Button color={isLogIn ? "inherit" : "success"} onClick={()=> setIsLogIn(false)}>Sign Up</Button>
              <Button color={isLogIn ? "success" : "inherit"} onClick={()=> setIsLogIn(true)}>Log In</Button>
            </ButtonGroup>
            {
              isLogIn ? <Login /> : <Signup />
            }
          </Grid>
        </Grid>
      </Container>
    </div>
  )
}
export default App;

