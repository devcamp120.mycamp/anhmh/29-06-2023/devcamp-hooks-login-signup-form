import { Grid, TextField, Typography, Button } from '@mui/material'
import React, { useState } from 'react'

function Signup() {
    const[firstname, setFirstname] = useState("");
    const[lastname, setLastname] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    return (
        <div>
            <Grid container mt={3} sx={{ justifyContent: "center", alignItems: "center" }}>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                    <Typography style={{ textAlign: "center" }} variant="h4" component="div">
                        Sign Up for Free!
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={12} md={6} lg={6} mt={2} paddingRight={1}>
                    <TextField id="input-firstname" fullWidth label="First Name" variant="outlined" onChange={(event) => setFirstname(event.target.value)} value={firstname}></TextField>
                </Grid>
                <Grid item xs={12} sm={12} md={6} lg={6} mt={2} paddingLeft={1}>
                    <TextField id="input-lastname" fullWidth label="Last Name" variant="outlined" onChange={(event) => setLastname(event.target.value)} value={lastname}></TextField>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12} mt={2}>
                    <TextField id="input-username" fullWidth label="Email Address" variant="outlined" onChange={(event) => setUsername(event.target.value)} value={username}></TextField>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12} mt={2}>
                    <TextField id="input-password" fullWidth label="Set A Password" variant="outlined" onChange={(event) => setPassword(event.target.value)} value={password}></TextField>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12} mt={2}>
                    <Button id="btn-signup" fullWidth color="success" variant="contained">Get Started</Button>
                </Grid>
            </Grid>
        </div>
    )
}

export default Signup

