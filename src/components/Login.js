import { Grid, TextField, Typography, Button } from '@mui/material'
import React, { useState } from 'react'


function Login() {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    return (
        <div>
            <Grid container mt={3} sx={{ justifyContent: "center", alignItems: "center" }}>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                    <Typography style={{ textAlign: "center" }} variant="h4" component="div">
                        Welcome Back!
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12} mt={2}>
                    <TextField id="input-username" fullWidth label="Email Address" variant="outlined" onChange={(event)=>setUsername(event.target.value)} value={username}></TextField>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12} mt={2}>
                    <TextField id="input-password" fullWidth label="Password" variant="outlined" onChange={(event)=>setPassword(event.target.value)} value={password}></TextField>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12} mt={2}>
                    <Button id="btn-login" fullWidth color="success" variant="contained">Log In</Button>
                </Grid>
            </Grid>
        </div>
    )
}

export default Login

